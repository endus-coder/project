<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
public function run()
{
    $data = [
        [
            'name'     => 'AuthorOne',
            'email'    => 'test@gmail.com',
            'password' => bcrypt('123123'),
        ],

        [
            'name'     => 'AuthorTwo',
            'email'    => 'test2@gmail.com',
            'password' => bcrypt('122343123'),
        ],

    ];
    DB::table('users')->insert($data);


}


}
