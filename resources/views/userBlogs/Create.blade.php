@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">

            <form action="">
                @csrf
                <div class="form-group">
                    <label for="usr">BlogName</label>
                    <input type="text" class="form-control" id="usr">
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>
            </form>


        </div>
    </div>
@endsection
