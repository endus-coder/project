
{{Request::header('Content-Type : text/xml')}}
@php echo '<?xml version="1.0" encoding="UTF-8" ?>'; @endphp
<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
    @foreach($posts as $post)
        <url>
            <loc>{{url($post->slug)}}</loc>
            <lastmod>{{$post->updated_at->tz('GMT')->toAtomString()}}</lastmod>
            <priority>1</priority>
        </url>
    @endforeach

        @foreach($categories as $category)
            <url>
                <loc>{{url($category->slug)}}</loc>
                <priority>1</priority>
            </url>
        @endforeach

</urlset>
