@if($errors->any())
    <div class="col-md-12">
        <div class="alert alert-danger" role="alert">

            <button class="btn btn-danger" class="close" data-dismiss="alert">
                <span aria-hidden="true">close</span>
            </button>
            @foreach($errors->all() as $errorText)
                <li>{{$errorText}}</li>
            @endforeach
        </div>
    </div>
@endif

@if(session('success'))

    <div class="col-md-12">
        <div class="alert alert-success" role="alert">

            <button  class="close" data-dismiss="alert">
                <span aria-hidden="true">close</span>
            </button>
            {{session()->get('success')}}
        </div>
    </div>


@endif
