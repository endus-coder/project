@extends('layouts.app')

@section('content')
    @php//@var App/Model/BlogPost  $item @endphp

        <div class="container">
          @include('blog.admin.posts.includes.result_mes')

            @if($item->exists)
     `          <form method="POST" action="{{route('Blog.admin.posts.update', $item->id)}}">
                    @method('PATCH')
                    @else
                        <form method="POST" action = "{{route('Blog.admin.posts.store')}}">
             @endif
                    @csrf
            <div class="row justify-content-center">
                <div class="col-md-8">
                    @include('blog.admin.posts.includes.item_edit_main_col')
                </div>

            </div>
        </div>




    </form>

        @if($item->exists)
            <br>
            <form method="POST" action="{{route('Blog.admin.posts.destroy', $item->id)}}">
                @method('DELETE')
                @csrf
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card card-block">
                            <div class="card-body ml-auto">
                                <button class="bnt bnt-link">Удалить</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        @endif









@endsection
