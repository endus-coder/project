@extends('layouts.app')
@php /** @var  $paginate App/Models/BlogPosts */ @endphp
@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
               @include('blog.admin.posts.includes.result_mes')
                <nav class="navbar navbar-toggleable-md navbar-light bd-faded">
                    <a href="{{route('Blog.admin.posts.create')}}" class="btn btn-primary">Добавить новый пост</a>
                </nav>
                <div class="card">
                    <div class="card-body">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Автор</th>
                                <th>Краткое описание</th>
                                <th>Категория</th>
                                <th>Заголовок</th>
                                <th>Дата публикаци</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($paginate as $post)
                               <tr @if ($post->is_published) style="background-color: whitesmoke" @endif>
                                   <td>{{$post->id}}</td>
                                   <td>{{$post->user->name}}</td>
                                   <td>{{$post->title}}</td>
                                   <td>{{$post->category->title}}</td>
                                    <td>
                                        <a href="{{route('Blog.admin.posts.edit',$post->id)}}">{{$post->title}}</a>
                                    </td>
                                   <td>{{$post->published_at ? \Carbon\Carbon::parse($post->published_at)->format('d.M H:i') : ''}}</td>
                               </tr>
                            @endforeach

                            </tbody>



                        </table>
                    </div>
                </div>


            </div>
        </div>
    </div>

    @if($paginate->total() > $paginate->count())
        <br>
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        {{$paginate->links()}}
                    </div>
                </div>
            </div>
            @endif
        </div>
@endsection


















