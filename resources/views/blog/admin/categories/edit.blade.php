@extends('layouts.app')

@section('content')
    @php //@var App/Model/BlogCategory $item @endphp

    @if($item->exists)
        <form action="{{route('Blog.admin.categories.update', $item->id)}}" method="POST">
        @method('PATCH')
    @else
        <form action="{{route('Blog.admin.categories.store')}}" method="POST">
    @endif
    @csrf
        <div class="container">
        @php
            /** @var /Illimate/Suppots/ViewErrorBag $errors*/
        @endphp

            @if($errors->any())
           <div class="col-md-12">
            <div class="alert alert-danger" role="alert">

                <button class="btn btn-danger close" data-dismiss="alert">
                    <span aria-hidden="true">close</span>
                </button>
            {{$errors->first()}}
            </div>
           </div>
            @endif

            @if(session('success'))

                <div class="col-md-12">
                    <div class="alert alert-success" role="alert">

                        <button  class="close" data-dismiss="alert">
                            <span aria-hidden="true">close</span>
                        </button>
                        {{session()->get('success')}}
                    </div>
                </div>


            @endif




            <div class="row justify-content-center">

                <div class="col-md-8">

                    @include('blog.admin.categories.includes.item_edit_main_col')


                </div>

                <div class="col-md-3">
{{--                @include('blog.admin.categories.includes.item_edit_add_col')--}}


                </div>

            </div>
    </div>
    </form>
@endsection
