<?php

namespace App\Console\Commands;

use App\Models\BlogPost;
use App\Models\User;
use Illuminate\Console\Command;

class Lists extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'blogs:list {--delete=default}?';



    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {


        if($this->option('delete') !== null && $this->option('delete') !== 'default') {
            if(BlogPost::destroy($this->option('delete'))){
                $this->info('пост успешно удален');
                die();
            }
            $this->error('Пост не найден или уже был удален');
            die();
        }


        if($posts = BlogPost::all()->where('id','>',3)->toArray()){
                dd(($posts));
        }
            $this->error('В базе данных еще нету постов');

    }


}
