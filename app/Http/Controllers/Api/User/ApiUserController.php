<?php

namespace App\Http\Controllers\User;

use App\Models\User;
use App\Notifications\RegisterMessage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;

class ApiUserController extends Controller
{
    public function login()
    {
        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            $user = Auth::user();
            $success['token'] = $user->createToken('MyAppToken')-> accessToken;
            return response()->json(['success' => $success], 200);
        }else{
            return response()->json(['error' => 'Unauthorized'],401);
        }


    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name'       => 'required',
            'email'      =>'required|email',
            'password'   => 'required',
            'c_password' => 'required|same:password',
        ]);
        if($validator->fails()){
            return response()->json(['error' => $validator->errors()],401);
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);

        $user->notify(new RegisterMessage($user->name));
        $success['token'] = $user->createToken('MyAppToken')->accessToken;
        $success['name'] = $user->name;

        return response()->json(['success' => $success], 200);
    }

    public function details()
    {
        $user = Auth::user();
        return response()->json(['success' => $user],200);
    }

    public function admin()
    {
        return response()->json(['success' => 'Админ успешно авторизован'],200);
    }


}
