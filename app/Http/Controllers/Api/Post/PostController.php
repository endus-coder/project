<?php

namespace App\Http\Controllers\Api\Post;

use App\Models\BlogPost;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PostController extends Controller
{

    public function getPosts()
    {
        $posts = BlogPost::paginate();
        return response()->json($posts,200);
    }


}
