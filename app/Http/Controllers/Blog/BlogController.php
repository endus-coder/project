<?php

namespace App\Http\Controllers\Blog;

use App\Models\BlogCategory;
use App\Models\BlogPost;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class BlogController extends Controller
{
    public function siteMap()
    {
        return Response::view('blog.all.site-map',['posts' =>BlogPost::get(),'categories'=> BlogCategory::get()])->header('Content-Type','text/xml');
    }
}
