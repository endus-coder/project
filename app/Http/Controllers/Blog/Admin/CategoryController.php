<?php

namespace App\Http\Controllers\Blog\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\BlogCategoryCreateRequest;
use App\Http\Requests\BlogCategoryUpdateRequest;
use App\Models\BlogCategory;
use Illuminate\Http\Request;


class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $item = new BlogCategory();
        $columns = ['id', 'title', 'parent_id'];
        $paginator = $item->select($columns)
                          ->with([
                             'parentCategory:id,title'
                          ])
                          ->paginate(5) ;



        return view('blog.admin.categories.index', compact('paginator'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $item = new BlogCategory();
        $categorylist = BlogCategory::all();

        return view('blog.admin.categories.edit', compact('item', 'categorylist'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlogCategoryCreateRequest $request)
    {
        $data = $request->input();

        $item = new BlogCategory($data);

        $item->save();

        if($item) {
            return redirect()
                ->route('blog.admin.categories.edit', $item->id)
                ->with(['success' => 'успешно сохранено']);
        }else{
            return back()
                ->withErrors(['message' => 'ошибка сохранения'])
                ->withInput();
        }


    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = BlogCategory::findOrFail($id);

        $categorylist = BlogCategory::all();

        return view('blog.admin.categories.edit',
        compact('item','categorylist'));



    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BlogCategoryUpdateRequest $request, $id)
    {

        $item = BlogCategory::find($id);

        if(empty($item)) {
            return back()
                ->withErrors(['message' =>"запись с id = {{$id}} не найдена"])
                ->withInput();
        }
        $data = $request->all();
        $result = $item
            ->fill($data)
            ->save();

        if($result) {
            return redirect()
                ->route('blog.admin.categories.edit', $item->id)
                ->with(['success' => 'успешно сохранено']);
        }else{
            return back()
                ->withErrors(['message' => 'ошибка сохранения'])
                ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

}
