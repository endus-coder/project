<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BlogCategory extends Model
{
    use SoftDeletes;

    protected $fillable = [
      'title',
      'slug',
      'parent_id',
      'description',
    ];

    public function parentCategory()
    {
        return $this->belongsTo(BlogCategory::class,'parent_id','id');
    }


    public function getTitleAttribute($valueFromObject)
    {
        return mb_strtoupper($valueFromObject);
    }


    public function setTitleAttribute($incomingValue)
    {
        $this->attributes['title'] = mb_strtolower($incomingValue);
    }


    public function getDescriptionAttribute($valueFromObject)
    {
        return mb_strtoupper($valueFromObject);
    }


    public function setDescriptionAttribute($incomingValue)
    {
        $this->attributes['title'] = mb_strtolower($incomingValue);
    }


}
