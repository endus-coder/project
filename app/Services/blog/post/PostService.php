<?php


namespace App\Services\blog\post;


use App\Http\Requests\BlogPostCreateRequest;
use App\Http\Requests\BlogPostUpdateRequest;
use App\Models\BlogCategory;
use App\Models\BlogPost;
use Illuminate\Support\Facades\Auth;

class PostService
{
    public static function createrPost()
    {
        $item = new BlogPost();
        $authorId = Auth::user()->id;
        $categoryList = BlogCategory::all();

        return view('blog.admin.posts.edit',
            compact('item','categoryList','authorId'));

    }

    public static function indexPost()
    {
        $res = new BlogPost();
        $paginate = $res->select(['id', 'title', 'slug', 'is_published', 'published_at', 'user_id', 'category_id'])
            ->orderBy('id', 'DESC')
            ->with([
                'category' => function ($query) {
                    $query->select(['id', 'title']);
                },
                'user:id,name',

            ])
            ->paginate(20);


        return view('blog.admin.posts.index', compact('paginate'));
    }



    public static function storePost(BlogPostCreateRequest $request)
    {
        $data = $request->input();
        $item = (new BlogPost())->create($data);
        if($item){
            return redirect()->route('Blog.admin.posts.edit', [$item->id])
                ->with(['success' => 'Сохранено']);
        }
        return back()->withErrors(['error_message' => 'ошибка при сохранении'])
            ->withInput();

    }



    public static function editPost($id)
    {
        $item = BlogPost::findOrFail($id);
        $categoryList = BlogCategory::all();
        $authorId = Auth::user()->id;
        if(Auth::user()->id == $item->user_id) {
            return view('blog.admin.posts.edit',
                compact('item','categoryList','authorId'));
        }else{
            return redirect('/');
        }
    }

    public static function updatePost(BlogPostUpdateRequest $request, $id)
    {
        $item = BlogPost::findOrFail($id);
        $data = $request->all();

        $result = $item->update($data);

        if($result) {
            return redirect()
                ->route('Blog.admin.posts.edit', $item->id)
                ->with(['success'=>'Сохранено успешно']);
        }
        return back()
            ->withErrors(['error_save' => 'ошибка сохранения'])
            ->withInput();
    }


    public static function destroyPost($id)
    {
        $result = BlogPost::destroy($id);

        if($result){
            return redirect()
                ->route('blog.admin.posts.index')
                ->with(['success' => 'запись с id' . $id . 'удалена']);
        } else {
            return back()->withErrors(['error_delete' => 'Ошибка удаления']);
        }
    }



    public static function showPost($id)
    {
        return null;
    }





}
