<?php

namespace App\Observers;

use App\Models\BlogPost;
use Carbon\Carbon;
use Str;

class BlogPostObserver
{




    /**
     * Handle the models blog post "created" event.
     *
     * @param  \App\Models\BlogPost  $modelsBlogPost
     * @return void
     */
    public function created(BlogPost $modelsBlogPost)
    {
        $this->setPublished($modelsBlogPost);
        $this->setSlug($modelsBlogPost);
        $this->setHtml($modelsBlogPost);

    }

    public function creating(BlogPost $modelsBlogPost)
    {
        $this->setPublished($modelsBlogPost);
        $this->setSlug($modelsBlogPost);
        $this->setHtml($modelsBlogPost);

    }

    public function updating(BlogPost $modelsBlogPost)
    {

        $this->setPublished($modelsBlogPost);
        $this->setSlug($modelsBlogPost);
        $this->setHtml($modelsBlogPost);


    }


    /**
     * Handle the models blog post "updated" event.
     *
     * @param  \App\Models\BlogPost  $modelsBlogPost
     * @return void
     */
    public function updated(BlogPost $modelsBlogPost)
    {

    }


    /**
     * @param BlogPost $modelsBlogPost
     */
    public function deleting(BlogPost $modelsBlogPost)
    {
        //return false;
    }



    /**
     * Handle the models blog post "deleted" event.
     *
     * @param  \App\Models\BlogPost  $modelsBlogPost
     * @return void
     */
    public function deleted(BlogPost $modelsBlogPost)
    {
        //
    }




    /**
     * Handle the models blog post "restored" event.
     *
     * @param  \App\Models\BlogPost  $modelsBlogPost
     * @return void
     */
    public function restored(BlogPost $modelsBlogPost)
    {
        //
    }

    /**
     * Handle the models blog post "force deleted" event.
     *
     * @param  \App\Models\BlogPost  $modelsBlogPost
     * @return void
     */
    public function forceDeleted(BlogPost $modelsBlogPost)
    {
        //
    }

    protected function setPublished(BlogPost $modelBlogPost)
    {

        if(empty($modelBlogPost->published_at) && $modelBlogPost->is_published) {
            $modelBlogPost->published_at = Carbon::now();
        }
    }

    protected function setSlug(BlogPost $modelBlogPost)
    {
        if(empty($modelBlogPost->slug)){
           if(BlogPost::all()->where('slug', Str::slug($modelBlogPost->title))){
               $modelBlogPost->slug = Str::slug($modelBlogPost->title) . str_random(21) . $modelBlogPost->category_id;
           }else{
               $modelBlogPost->slug = Str::slug($modelBlogPost->title);
           }
        }

    }

    protected function setHtml(BlogPost $modelBlogpost)
    {
        if($modelBlogpost->isDirty('content_raw')) {
            $modelBlogpost->content_html = $modelBlogpost->content_raw;
        }
    }





}
