<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


//Route::prefix('/posts')->group(function (){
//
//});
//getPosts
Route::get('/all_posts','Api\Post\PostController@getPosts');



//Route::post('login','Api\User\ApiUserController@login');
//Route::post('register', 'Api\User\ApiUserController@register');
//Route::group(['middleware' => 'auth:api'], function (){
//Route::post('details','Api\User\ApiUserController@details');
//});

Route::post('updateToken','Api\User\ApiTokenController@update');


Route::middleware('auth:api')->get('/userProof', function(Request $request) {
    return $request->user();
});




//Route::get('admin/profile','User\ApiUserController@adminController' )->middleware('auth');
